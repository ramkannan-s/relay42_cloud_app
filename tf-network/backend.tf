terraform {
  backend "s3" {
    bucket = "relay42-terraform-s3-west"
    key    = "network/terraform.tfstate"
    region = "eu-west-1"
  }
}