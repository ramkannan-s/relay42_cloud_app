region = "eu-west-1"

environment = "prod"

app_ami_id = "ami-08ca3fed11864d6bb"

bastion_ami_id = "ami-04f5641b0d178a27a"

bastion_instance_type = "t2.large"

app_instance_type = "t3.micro"

key_pair_name = "experiment-relay42-key-pair"

bastion_instance_count = "1"

app_instance_count = "3"

app_volume_size = "15"

bastion_volume_size = "10"
